#include<stdio.h>
int main()
{
    int arr[20];
    int n, i, new, pos;
    
    printf("\n Enter the size of the array: ");
    scanf("%d",&n);
    
    for(i=0; i<n; i++)
    {
        printf("\nEnter the element number %d: ", i+1);
        scanf("%d", &arr[i]);
    }
    printf("\n The array is: ");
    for(i=0; i<n; i++)
    {
        printf("%d\t", arr[i]);
    }
    
    printf("\n\n Enter the new element and its position: ");
    scanf("%d %d", &new, &pos);
    
    for(i=n-1; i>=pos-1; i--)
    {
        arr[i+1]=arr[i];
    }
    arr[pos-1]=new;
    n++;
    
    printf("\n The array after insertion of new element is: ");
    for(i=0; i<n; i++)
    {
        printf("%d\t", arr[i]);
    }
return 0;
}