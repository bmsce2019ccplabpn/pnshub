#include <stdio.h>

int fact(int x)
{
    int sum=1;
    for(int i=1;i<=x;i++)
    {
        sum=sum*i;
    }
    return sum;

}

void output(int x, int y)
{
    printf("\ The factorial of %d is = %d \n", x,y);
}

int main()
{
    int n, factorial;
    printf("\n Enter the number whose factorial has to be calculated: ");
    scanf("%d", &n);
    factorial=fact(n);
    output(n, factorial);
    return 0;
}